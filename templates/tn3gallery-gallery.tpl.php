<?php
/**
 *	
 *	Main TN3 Gallery
 *	
 *	theme function: includes/tn3gallery.inc
 *	
 *	
 */
	global $base_path;
	$pathtomodule = drupal_get_path('module', 'tn3gallery');
	$serverRoot = $_SERVER['DOCUMENT_ROOT'];
	
	
	// 	CONVERT TO A FUNCTION  IN BPORTRAITS MODULE//
	//path to directory to scan
	$directory = $serverRoot . $base_path . $pathtomodule . "/images/";
	
	//get all image files with a .jpg extension.
	$imagesRoot = glob($directory . "*.jpg");
	
	//REMOVE ROOT PATH
	foreach($imagesRoot as $imageRoot){
		$images[] = str_replace($serverRoot, "", $imageRoot);
			
	}
	
	// 	BRING TO A FUNCTION //
	
?>


    <div class="mygallery">
	<div class="tn3 album">
	    <ol>
	
	<?php foreach($images as $image){ ?>
		
		<li>
		    <h4></h4>
		    <div class="tn3 description"></div>
		    <a href="<?php echo $image ?>">
			<img src="<?php echo $image ?>" />
		    </a>
		</li>
		
		
	<?php }	?>
			
	    </ol>
	</div>
	</div>
	




